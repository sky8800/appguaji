import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JifenxiangqingPage } from './jifenxiangqing.page';

describe('JifenxiangqingPage', () => {
  let component: JifenxiangqingPage;
  let fixture: ComponentFixture<JifenxiangqingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JifenxiangqingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JifenxiangqingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
