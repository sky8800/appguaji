import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { JifenxiangqingPage } from './jifenxiangqing.page';

const routes: Routes = [
  {
    path: '',
    component: JifenxiangqingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [JifenxiangqingPage]
})
export class JifenxiangqingPageModule {}
