import { Injectable } from '@angular/core';


import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpservicesService {

  constructor(public http:HttpClient) { }
  
  get(api){
    return new Promise((re,refs)=>{
      this.http.get(api).subscribe((res)=>{
          re(res)
      },(err)=>{
          refs(err)
      })
    })
  }
}
